Il protocollo è abbastanza stupido, non ha bisogno d'essere troppo efficiente ma sarebbe comodo farlo compatibile tra i vari linguaggi. 

Quello che vogliamo usare è un semplice telnet che invia statement divisi da \n.

Ogni statement inviato è formato da comando e parametri divisi da spazio, es "name Giovanni" dove “name” è il comando e “Giovanni” è l’argomento. 
Per gli statement con argomenti negli spazi si deve includere la stringa in doppi apici e escapare i caratteri speciali (i doppi apici e lo slash) con uno slash, es: ‘luca disse “ciao” a mario’ diventa “luca disse \”ciao\” a mario”, stessa cosa con gli slash.

I comandi presenti finora sono: 

# Client » Server

## Fase di joining

**name &lt;player_name&gt;**

**request_start**

Usato per startare il gioco. Funziona solo se c’è più di un player.

## Fase di gioco

**rate &lt;rating&gt;**

Comando usato per votare il giocatore che sta eseguendo la sua prova. Una volta che sono stati ricevuti tutti i voti il gioco si ferma.

**end_joke**

Comando usato per finire una battuta, usato soltanto quando il tema è "battuta" e mandato solo dal giocatore che sta facendo la corrida

## Fase di fine gioco

**request_restart**

Crea una richiesta per ricominciare il gioco dalla lobby, se tutto va bene dovrebbe inviare a tutti i client un comando restart

# Server » Client

## Fase di joining

**name_response &lt;ok|taken&gt;**

Indica se la richiesta di nome è andata a buon fine o il nome è stato già preso

**players &lt;player_name&gt;...**

**player_join &lt;player_name&gt;**

**player_quit &lt;player_name&gt;**

**performance_start &lt;player_name&gt;**

Inizia il turno per il giocatore.

## Fase di gioco

**performance_theme &lt;type&gt;**

Il tema viene inviato non appena il giocatore lo sceglie dicendolo a voce al NAO.

Tipi di prove che il giocatore può sostenere:
- canto
- danza
- battuta

**rate_start**

Comincia la fase di voto

**rate_end &lt;score&gt;**

Finisce la fase di voto definendo il voto dato al giocatore

**game_end**

Cambia la fase di gioco in END_GAME

## Fase di end_game

**restart**

Inviato in seguito a un **request_restart** questo comando riporta il gioco alla fase di joining. In seguito al comando viene anche inviato un **players** command per listare tutti i giocatori presenti nella nuova lobby

## Qualsiasi fase

**error &lt;type&gt; &lt;description&gt;**

Ci sono diversi tipi di errore:
- wrong_state: Un comando è stato eseguito in uno stato sbagliato (e.g. usare start quando il gioco è già partito)
- argument_error: Errore negli argomenti del comando
- command_not_found: Il comando non è stato trovato
- cannot_start: Chiamato quando non è possibile cominciare il gioco
- illegal_player: L’operazione non può essere eseguita dal player che ha mandato la richiesta

Quando il client invia il nome al nao, se il nao non gli invia una risposta subito (perché sta aspettando che ulteriori giocatori si connettano), il client visualizza una schermata di attesa finché non riceverà i dati di gioco.
